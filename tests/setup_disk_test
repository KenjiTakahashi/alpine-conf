#!/usr/bin/env atf-sh

. $(atf_get_srcdir)/test_env.sh
init_tests \
	setup_disk_usage \
	setup_disk_mode_none \
	setup_disk_none \
	setup_disk_func_find_disks \
	setup_disk_func_setup_partitions_dos \
	setup_disk_func_setup_partitions_gpt \
	setup_disk_func_find_efi_size \
	setup_disk_non_existing_block_dev

setup_disk_usage_body() {
	test_usage setup-disk
}

setup_disk_mode_none_body() {
	init_env
	atf_check -s exit:0 \
		setup-disk -m none
}

setup_disk_none_body() {
	init_env
	atf_check -s exit:0 \
		setup-disk none
}

fake_disk() {
	local dev="$1"
	local size="${2:-1000}" # size in MiB
	local blocksize="${3:-512}"

	mkdir -p dev \
		sys/block/$dev/device \
		sys/block/$dev/holders \
		sys/block/$dev/queue

	echo $(( $size * 1000000 / $blocksize )) > sys/block/$dev/size
	echo $(( $blocksize )) > sys/block/$dev/queue/logical_block_size
}


fake_partition() {
	mkdir -p sys/block/$1/$1$2/holders
}

fake_mount() {
	mkdir -p proc
	echo "$1" >> proc/mounts
}

fake_raid() {
	local md="$1"
	shift
	for dev; do
		for p in sys/block/*/$dev/holders sys/block/$dev/holders; do
			if [ -d "$p" ]; then
				touch "$p"/$md
			fi
		done
	done
}

setup_disk_func_find_disks_body() {
	init_env
	fake_disk vda
	fake_disk vdb
	fake_partition vdb 1
	fake_disk sda
	fake_disk nvme0n1
	fake_partition nvme0n1 p2

	# simulate vda and vdb1 being part of md0 raid
	fake_raid md0 vda vdb1

	# simulate nvme0n1p2 being mounted
	fake_mount '/dev/nvme0n1p2 /boot vfat rw,relatime,fmask=0022 0 0'

	SETUP_DISK_TESTFUNC=find_disks USE_RAID=1 \
		atf_check -s exit:0 \
		-o match:"^ sda$" \
		-e match:"vda is part of a running raid" \
		-e match:"vdb is part of a running raid" \
		setup-disk
}

setup_disk_func_setup_partitions_dos_body() {
	atf_require_prog sfdisk
	init_env

	mkdir -p dev
	truncate -s 100M dev/vda

	SETUP_DISK_TESTFUNC=setup_partitions  \
		atf_check -s exit:0 \
		-o match:"mdev" \
		setup-disk dev/vda "0M," "10M,83,*", "10M,82", "10M,fd", ",8e"

	SETUP_DISK_TESTFUNC=find_partitions  \
		atf_check -s exit:0 \
		-o match:"dev/vda1" \
		setup-disk dev/vda "boot"

	SETUP_DISK_TESTFUNC=find_partitions  \
		atf_check -s exit:0 \
		-o match:"dev/vda1" \
		setup-disk dev/vda "linux"

	SETUP_DISK_TESTFUNC=find_partitions  \
		atf_check -s exit:0 \
		-o match:"dev/vda2" \
		setup-disk dev/vda "swap"

	SETUP_DISK_TESTFUNC=find_partitions  \
		atf_check -s exit:0 \
		-o match:"dev/vda3" \
		setup-disk dev/vda "raid"

	SETUP_DISK_TESTFUNC=find_partitions  \
		atf_check -s exit:0 \
		-o match:"dev/vda4" \
		setup-disk dev/vda "lvm"
}

setup_disk_func_setup_partitions_gpt_body() {
	atf_require_prog sfdisk
	init_env

	mkdir -p dev
	truncate -s 200M dev/vda

	swap=0657FD6D-A4AB-43C4-84E5-0933C84B4F4F
	linux=0FC63DAF-8483-4772-8E79-3D69D8477DE4
	raid=A19D880F-05FC-4D3B-A006-743F0F84911E
	lvm=E6D6D379-F507-44C2-A23C-238F2A3DF928
	prep=9E1A2d38-C612-4316-AA26-8B49521E5A8B
	esp=C12A7328-F81F-11D2-BA4B-00A0C93EC93B

	SETUP_DISK_TESTFUNC=setup_partitions DISKLABEL=gpt \
		atf_check -s exit:0 \
		-o match:"mdev" \
		setup-disk dev/vda "0M," "30M,$esp" "30M,$swap" "30M,$linux" "30M,$raid" "30M,$lvm"

	sfdisk -d dev/vda

	SETUP_DISK_TESTFUNC=find_partitions DISKLABEL=gpt USE_EFI=1 \
		atf_check -s exit:0 \
		-o match:"dev/vda1" \
		setup-disk dev/vda "boot"

	SETUP_DISK_TESTFUNC=find_partitions DISKLABEL=gpt \
		atf_check -s exit:0 \
		-o match:"dev/vda2" \
		setup-disk dev/vda "swap"

	SETUP_DISK_TESTFUNC=find_partitions DISKLABEL=gpt \
		atf_check -s exit:0 \
		-o match:"dev/vda3" \
		setup-disk dev/vda "linux"

	SETUP_DISK_TESTFUNC=find_partitions DISKLABEL=gpt \
		atf_check -s exit:0 \
		-o match:"dev/vda4" \
		setup-disk dev/vda "raid"

	SETUP_DISK_TESTFUNC=find_partitions DISKLABEL=gpt \
		atf_check -s exit:0 \
		-o match:"dev/vda5" \
		setup-disk dev/vda "lvm"
}

setup_disk_func_find_efi_size_body() {
	init_env
	# size:blocksize:expected
	for i in 10000::512 4000::264 1000::160 \
			10000:4096:512 4000:4096:264 1000:4096:264; do

		local diskdata=${i%:*}
		local disksize=${diskdata%:*}
		local blocksize=${diskdata#*:}
		local expected=${i##*:}

		fake_disk vda $disksize $blocksize
		SETUP_DISK_TESTFUNC=find_efi_size \
			atf_check -s exit:0 \
			-o match:"^$expected$" \
			setup-disk /dev/vda
	done
}

setup_disk_non_existing_block_dev_body() {
	init_env
	atf_check -s not-exit:0 \
		-e match:"/dev/vda is not a block device suitable for partitioning" \
		-o match:"swapoff" \
		setup-disk -m sys /dev/vda
}

